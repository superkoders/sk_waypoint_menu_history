{{#sprites}}
sprite2x-{{name}}()
	background url('{{../baseUrl}}{{../fileName}}?{{../timestamp}}') unit(-{{x}} / 2, 'px') unit(-{{y}} / 2, 'px') no-repeat
	background-size unit({{../width}} / 2, 'px') unit({{../height}} / 2, 'px')
sprite2x-{{name}}-size()
	sprite2x-{{name}}()
	size ({{width}} / 2) ({{height}} / 2)
sprite2x-{{name}}-position()
	background-position unit(-{{x}} / 2, 'px') unit(-{{y}} / 2, 'px')
{{/sprites}}