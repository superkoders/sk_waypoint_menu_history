{{#sprites}}
sprite-{{name}}()
	background url('{{../baseUrl}}{{../fileName}}?{{../timestamp}}') -{{x}}px -{{y}}px no-repeat
sprite-{{name}}-size()
	sprite-{{name}}()
	size {{width}} {{height}}
sprite-{{name}}-position()
	background-position -{{x}}px -{{y}}px
{{/sprites}}