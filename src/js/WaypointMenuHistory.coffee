$ = require('jquery')
window.jQuery ?= $

require('sk-jquery-browser')
require('sk-jquery-bbq')

WaypointMenu = require('sk-waypoint-menu')

class WaypointMenuHistory extends WaypointMenu
	constructor: (options) ->
		super

		@options = $.extend(@options,
			historyPrefix: 'page'
		)

	init: ->
		@one('beforeinit', =>
			@$W.on('hashchange', @handleHash)
			@handleHash()
		)

		return super

	destroy: ->
		@one('destroy', =>
			@$W.off('hashchange', @handleHash)
		)

		return super

	handleHash: (event) =>
		event?.preventDefault()

		unless @byWaypoint
			hash = $.bbq.getState(@options.historyPrefix) or ''
			if hash
				if @isInit
					@scrollTo('#' + hash)
				else
					@setActual(hash)
					old = @options.scrollDuration
					@options.scrollDuration = 0
					@scrollTo('#' + hash)
					@options.scrollDuration = old

		delete @byWaypoint
		return

	handleClick: (event) =>
		event.preventDefault()

		hash = $(event.currentTarget).prop('hash').slice(1)
		@setHash(hash)

		return

	setHash: (hash) ->
		state = {}
		state[@options.historyPrefix] = hash
		$.bbq.pushState(state)

		return

	setActual: (element, isScroll) ->
		if not @isBusy and not isScroll
			@one('beforechange', =>
				@byWaypoint = true
				@setHash(element.id)
			)
		super
		return

module.exports = WaypointMenuHistory
